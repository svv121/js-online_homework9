"use strict";
/*
1. Метод document.createElement(tagName) дозволяє створити елемент з тим тегом, що вказано в аргументі.
2. Метод targetElement.insertAdjacentHTML(position, text) дозволяє вставити text (рядок HTML коду) у будь-яке місце сторінки щодо цільового елемента. Можна зробити вставку перед цільовим елементом (position = beforeBegin), після нього (position = afterEnd), а також на початок (position = afterBegin) або кінець (position = beforeEnd) цільового елемента.
3. Елемент із DOM-дерева можна видалити за допомогою методу Element.remove().
*/
const regions = [24, ["Cherkaska", "Chernihivska", "Chernivetska", "Dnipropetrovska", "Donetska", "Ivano-Frankivska", "Kharkivska", "Khersonska", "Khmelnytska", "Kirovohradska", "Kyivska", "Luhanska", "Lvivska", "Mykolaivska", "Odeska", "Poltavska", "Rivnenska", "Sumska", "Ternopilska", "Vinnytska", "Volynska", "Zakarpatska", "Zaporizka", "Zhytomyrska"], 1, ["Crimea"], 2, ["Kyiv", "Sevastopol"],];

function addArrayList(array, querySelector) {
    const arrayMap = array.map(entry => {
    if(Array.isArray(entry)){
       const entryMap = entry.map(elem=>`<li class = "list-inner-reg">${elem}</li>`);
       return `<ul class = "list-insert">${entryMap.join("")}</ul>`
    }
       return `<li>${entry}</li>`});
    const ulInsert = `<ul class = "list-insert">${arrayMap.join("")}</ul>`;
    return document.querySelector(querySelector).insertAdjacentHTML('beforeend', ulInsert);
}
addArrayList(regions, "#regions");

//---------------------second method--------------------------------
const cities = ["Kiev", ["Borispol", "Brovary", "Irpin", "Bucha", "Gostomel", "Vyshneve", "Vyshhorod",
    "Ukrayinka", "Vorzel",],"Kharkiv", "Odessa", "Dnipro","Donetsk", "Zaporizhzhya", "Lviv", "Kryvyi Rih",]

function addComplexArrayList(array, querySelector){
    const ulCreated = document.createElement('ul');
    ulCreated.classList.add("list-insert");
    document.querySelector(querySelector).append(ulCreated);
    array.forEach(elem => {
        const liCreated = document.createElement('li');
        ulCreated.append(liCreated);
        liCreated.textContent = elem;
        if(Array.isArray(elem)){
            liCreated.classList.add("list-inner");
            liCreated.textContent = "";
            addComplexArrayList(elem, ".list-inner");
        }
    })
}
addComplexArrayList(cities, "#cities");

function removeAll(){
    document.body.remove();
}
function clearPage(){
    let timeOut;
    do {
        timeOut = +prompt("Please, set time in seconds for clearing this page:", "3") * 1000;
    } while (!isFinite(timeOut) || timeOut <= 0);
    const countDownStart = new Date().getTime() + timeOut;
    const interval = setInterval(function() {
        const now = new Date().getTime();
        const range = countDownStart - now;
        const seconds = Math.floor(range / 1000) + 1;
        document.getElementById("countdown").textContent = "The page will be cleared in  " + seconds + " seconds";
        if (range < 0) {
            clearInterval(interval);
        }
    }, 100);
    setTimeout(removeAll, timeOut);
}